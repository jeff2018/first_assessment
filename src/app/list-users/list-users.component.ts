import { Component, OnInit, Input } from '@angular/core';
import { Regi } from '../shar/regi';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  @Input() users: Regi[];
  title :string = "List of Users"
  constructor() { }

  ngOnInit() {
  }

}
