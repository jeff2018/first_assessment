//Injects function from other files into this typescript file.
import { Component, OnInit } from '@angular/core';
import { Regi } from './shar/regi';
import { UserRegistrationService } from './services/user-registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showSingaporeCitizenId = false;
  title = 'app';
  model = null;
  users: any;

  isSubmitted: boolean = false;
  result: string = "";

  constructor(private userService: UserRegistrationService){

  }
  
  //Initialization declaration for the user registration model.
  ngOnInit() {
    this.model = new Regi('','','','','','',null,'','SG','',0,'','');
  }

  //The radio button values of the gender field is derived from the array.
  gender: string[] = ['Male', 'Female', 'Unisex', 'Alien'];
  //nationalities select list require to an array.
  nationalities = [ {desc:'Singapore', value:'SG'}, 
                    {desc: 'Malaysia', value:'MY'}, 
                    {desc:'Thailand', value:'TH'},  
                    {desc:'Brunei Darussalam', value: 'BN'},
                    {desc:'Viet Nam', value: 'VN'},
                    {desc:'Cambodia', value: 'KH'},
                    {desc:'Indonesia', value: 'ID'},
                    {desc:'Lao PDR', value: 'LA'},
                    {desc:'Myanmar', value: 'MM'},
                    {desc:'Philippines', value: 'PH'}];

  //toggle the isSubmitted flag to true display
  onSubmit(){
    console.log(this.model.email);
    console.log(this.model.password);
    console.log(this.model.confirmPassword);
    console.log(this.model.firstName);
    console.log(this.model.lastName);
    console.log(this.model.gender);
    console.log(this.model.dateOfBirth);
    console.log(this.model.address);
    console.log(this.model.nationality);
    console.log(this.model.contactNumber);
    console.log(this.model.age);
    console.log(this.model.idNo);
    console.log(this.model.friend);
    this.isSubmitted = true;
    this.userService.saveUserRegistration(this.model)
      .subscribe(users => {
        console.log('send to backend !');
        console.log(users);
        this.users = users;
      })
  }

  //onChange function to handle the on change event.
  onChange(event){

  }

}