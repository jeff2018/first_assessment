import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { environment } from '../../environments/environment';
import {Observable} from 'rxjs/Observable';

import { Regi } from '../shar/regi';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable()
export class UserRegistrationService {
  private userAPIURL = `${environment.backendApiURL}/api/user/` 
  private productAPIURL = `${environment.backendApiURL}/api/product/` 
  
  constructor(private httpClient: HttpClient) { }


  public saveUserRegistration(user: Regi) : Observable<Regi>{
    return this.httpClient.post<Regi>(this.userAPIURL + 'register', user, httpOptions);
  }


  public getAllReviews(){

  }
}
